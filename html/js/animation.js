function checkOverflowBox(element) {
  let el = document.querySelectorAll(element);
  el.forEach((box) => {
    let height = box.scrollHeight;
    box.style.setProperty('--mh-auto', height + 'px');
  });
}

function setOverflowBox(element) {
  if (document.querySelectorAll(element).length) {
    checkOverflowBox(element);
    window.addEventListener('resize', () => {
      checkOverflowBox(element);
    });
  }
}
function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}
ready(() => {
  if (window.matchMedia('(max-width: 1024px)').matches) {
    document.querySelector('.main-animation').classList.remove('loading');
    document.querySelector('.main-animation__video').classList.add('active');
    document.querySelector('.main-animation__tab').classList.add('active');  
    setOverflowBox('.main-animation__box');

    document.querySelectorAll('.main-animation__link').forEach((toggle) => {
      toggle.addEventListener('click', (e) => {
        e.preventDefault();
        document.querySelector('.main-animation').classList.add('transition-on');
        setTimeout(() => {
          document.querySelector('.main-animation').classList.remove('transition-on');
        }, 500);
        let boxExpand = toggle.getAttribute('data-slide');
        let container = document.querySelectorAll(boxExpand);
        document.querySelectorAll('.main-animation__tab').forEach((box) => {
          box.classList.remove('active');
        });
        document.querySelectorAll('.main-animation__video').forEach((box) => {
          box.classList.remove('active');      
        });
        container.forEach((box) => {
          box.classList.add('active');
        });
      });
    });
  }
  else {
    let vid = document.querySelector('video');
    document.querySelector('.main-animation').classList.remove('loading');
    document.querySelector('.main-animation__video').classList.add('active');
    vid.play();
    document.querySelector('.main-animation').classList.add('video-playing');
    vid.onended = function(e) {
      document.querySelector('.main-animation').classList.remove('video-playing');
    };
    setTimeout(() => {
      document.querySelector('.main-animation__tab').classList.add('active');
    }, 1000);
    setOverflowBox('.main-animation__box');
    document.querySelectorAll('.main-animation__link').forEach((toggle) => {
      toggle.addEventListener('click', (e) => {
        e.preventDefault();
        document.querySelector('.main-animation').classList.add('transition-on');
        setTimeout(() => {
          document.querySelector('.main-animation').classList.remove('transition-on');
        }, 1500);
        let boxExpand = toggle.getAttribute('data-slide');
        let container = document.querySelectorAll(boxExpand);
        document.querySelectorAll('.main-animation__tab').forEach((box) => {
          box.classList.remove('active');
        });
        document.querySelectorAll('.main-animation__video').forEach((box) => {
          box.classList.remove('active');      
        });
        container.forEach((box) => {
          box.classList.add('active');
          if (box.querySelectorAll('video').length) {
            document.querySelector('.main-animation').classList.add('video-playing');
            let video = box.querySelector('video');
            video.currentTime = 0;
            video.play();
            video.onended = function(e) {
              document.querySelector('.main-animation').classList.remove('video-playing');
            };
          }
        });
      });
    });
  }
});