/* Функции проверки заполнения полей формы для сдвига подписей.
 * Если форма добавляется AJAX, то этот блок надо перенести в инициализацию формы */
function checkValue(control) {
  if (control.value === "") {
    control.classList.remove("has-value");
  } else {
    control.classList.add("has-value");
  }
}
function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}
ready(() => {

  if (document.querySelectorAll(".form-control").length) {
    let $i = document.querySelectorAll(
      'input:not([type="checkbox"]):not([type="radio"]), textarea'
    );
    $i.forEach((control) => {
      checkValue(control);
      control.addEventListener("blur", () => {
        checkValue(control);
      });
      control.addEventListener("change", () => {
        checkValue(control);
      });
    });
  }
  
  /* Обработка кастомного контрола выбора файла */
  let inputs = document.querySelectorAll('input[type="file"]');
  inputs.forEach((input) => {
    let label = input.nextElementSibling,
      labelVal = label.innerHTML;
  
    input.addEventListener("change", (e) => {
      let fileName = "";
      if (input.files && input.files.length > 1)
        fileName = (input.getAttribute("data-multiple-caption") || "").replace(
          "{count}",
          input.files.length
        );
      else fileName = e.target.value.split("\\").pop();
  
      if (fileName) label.innerHTML = fileName;
      else label.innerHTML = labelVal;
    });
  });
  
  if (document.querySelectorAll(".selectbox").length) {
    document.querySelectorAll(".selectbox").forEach((select) => {
      document.querySelectorAll(".selectbox__option").forEach((item) => {
        item.addEventListener("click", () => {
          document.querySelectorAll(".form-control").forEach((control) => {
            if (control.contains(select)) {
              control.classList.add("selected");
            }
          });
        });
      });
    });
  }
  
  /* Добавление маски ввода к инпутам inputmode=tel */
  if (document.querySelectorAll('input[inputmode="tel"]').length) {
    document.querySelectorAll('input[inputmode="tel"]').forEach((input) => {
      Inputmask({
        mask: "+7 (999) 999-99-99",
        showMaskOnFocus: true,
        showMaskOnHover: false,
      }).mask(input);
    });
  }
  
  /* Функция открытия и закрытия модального окна. Любые модалки должны быть вне <div class="app"> */
  if (document.querySelectorAll("[data-modal]").length) {
    document.querySelectorAll("[data-modal]").forEach((modal) => {
      modal.addEventListener("click", (e) => {
        e.preventDefault();
        let modalContainer = modal.getAttribute("data-modal");
        initialScroll = window.scrollY;
        let $m = document.querySelector(modalContainer);
        $m.classList.toggle("opened");
        if (!$b.classList.contains("menu-on")) {
          if ($b.classList.contains("modal-on")) {
            $b.classList.remove("modal-on");
            let backScroll = $a.style.top.substring(1, $a.style.top.length - 2);
            $a.style.top = "";
            window.scrollTo({
              top: backScroll,
            });
          } else {
            $b.classList.add("modal-on");
            window.scrollTo({
              top: 0,
            });
            $a.style.top = "-" + initialScroll + "px";
          }
        } 
        let $s = document
            .querySelector('.modal-search')
            .querySelector('input[type="text"]');
        if ($b.classList.contains('modal-on')) {
          $s.focus();
        } else {
          $s.blur();
        }
      });
    });
  }
  
  /* Функция закрытия модалки по ESC */
  document.addEventListener("keyup", (e) => {
    e = e || window.event;
    let isEscape = false;
    if ("key" in e) {
      isEscape = e.key === "Escape" || e.key === "Esc";
    } else {
      isEscape = e.keyCode === 27;
    }
    if (isEscape) {
      $b.classList.remove("modal-on");
      document.querySelectorAll(".modal").forEach((modal) => {
        modal.classList.remove("opened");
      });
      let backScroll = $a.style.top.substring(1, $a.style.top.length - 2);
      $a.style.top = "";
      window.scrollTo({
        top: backScroll,
      });
    }
  });
  
  /* Функция закрытия модалки по клику вне. */
  document.querySelectorAll(".modal").forEach((modal) => {
    modal.addEventListener("click", (e) => {
      modal.querySelectorAll(".modal__box").forEach((box) => {
        if (!box.contains(e.target)) {
          e.preventDefault();
          document.querySelectorAll(".opened").forEach((opened) => {
            opened.classList.remove("opened");
          });
          let $o = document.querySelectorAll(".opened");
          if ($o.length === 0) {
            $b.classList.remove("modal-on");
            let backScroll = $a.style.top.substring(1, $a.style.top.length - 2);
            $a.style.top = "";
            window.scrollTo({
              top: backScroll,
            });
          }
        }
      });
    });
  });
});
